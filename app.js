const quizData = [
    {
        question: 'Capital du MAROC?',
        a: 'Casablanca',
        b: 'Rabat',
        c: 'Agadir',
        d: 'Marrakech',
        correct: 'd'
    }, {
        question: 'Plat principal au MAROC?',
        a: 'Tajine',
        b: 'Couscous',
        c: 'Rfissa',
        d: 'Pastilat',
        correct: 'a'
    }, {
        question: 'La plus grande ville au MAROC ',
        a: 'Casablanca',
        b: 'Rabat',
        c: 'Fez',
        d: 'Marrakech',
        correct: 'a'
    }, {
        question: 'Quels sont les pays frontaliers du MAROC?',
        a: 'La tunisie',
        b: 'Algerie',
        c: 'Mauritanie',
        d: 'Espagne',
        correct: 'b'
    }, {
        question: 'Le Roi du MAROC?',
        a: 'Mohamed6',
        b: 'Hassan2',
        c: 'Mohamed5',
        d: 'Moulay Hassan',
        correct: 'a'
    }
]

const quizData2 =[
    {
        question :'Capital de la FRANCE?',
        a:'Paris',
        b:'Marseille',
        c:'Lyon',
        d:'Toulouse',
        correct:'a'
    },{
        question :'Plat principal en FRANCE?',
        a:'Pizza',
        b:'Choucroute',
        c:'Bourguinion',
        d:'Moule Frites',
        correct:'c' 
    },{
        question :'La plus grande ville de France',
        a:'Paris',
        b:'Marseille',
        c:'Lyon',
        d:'Toulouse',
        correct:'a'
    },{
        question :'Quels sont les pays frontaliers de la France?',
        a:'Croatie',
        b:'Autriche',
        c:'Finlande',
        d:'Espagne',
        correct:'d'
    },{
        question :'Monnaie européenne?',
        a:'Euros',
        b:'Dollar',
        c:'francs',
        d:'Livre sterling',
        correct:'a'
    }
]
const quizData3 =[
    {
        question :'Capital du Japon?',
        a:'Tokyo',
        b:'Osaka',
        c:'Sapporo',
        d:'Chiba',
        correct:'a'
    },{
        question :'Plat principal en Japon?',
        a:'Sushi',
        b:'Tokoyaki',
        c:'Soupe miso',
        d:'Moule Frites',
        correct:'c' 
    },{
        question :'La plus grande ville du Japon',
        a:'Tokyo',
        b:'Hiroshima',
        c:'Sappora',
        d:'Toulouse',
        correct:'a'
    },{
        question :'Quels sont les pays frontaliers du Japon?',
        a:'Taïwan',
        b:'Russie',
        c:'Finlande',
        d:'Corée du sud',
        correct:'d'
    },{
        question :'Monnaie la plus utilisée au Japon?',
        a:'Yen',
        b:'Dollar',
        c:'francs',
        d:'Livre sterling',
        correct:'a'
    }
]


const answerEls = document.querySelectorAll('input[name="answer"]');
const questionE1 = document.getElementById('question');

const a_text = document.getElementById('a_text');
const b_text = document.getElementById('b_text');
const c_text = document.getElementById('c_text');
const d_text = document.getElementById('d_text');
const submitBtn = document.getElementById('Submit');

const quiz = document.querySelector("#quiz")
const quiz2 = document.querySelector("#quiz2")
const quiz3 = document.querySelector("#quiz3")


let currentQuiz = 0;
let score = 0;


function loadQuiz() {

    deselectAnswers();

    const currentQuizData = quizData[currentQuiz];

    questionE1.innerText = currentQuizData.question;

    a_text.innerText = currentQuizData.a;
    b_text.innerText = currentQuizData.b;
    c_text.innerText = currentQuizData.c;
    d_text.innerText = currentQuizData.d;
}
loadQuiz();

function getSelected() {
    let answer = undefined;

    answerEls.forEach((answerEl) => {
        if (answerEl.checked) {
            answer = answerEl.id;
        }
    });
    return answer;
}

function deselectAnswers() {

    answerEls.forEach((answerEl) => {
        answerEl.checked = false;
    });
}

submitBtn.addEventListener('click', () => {
    // check  answer
    const answer = getSelected();
    console.log("a");
    if (answer) {
        if (answer === quizData[currentQuiz].correct) {
            score++;
        }
        currentQuiz++;
        console.log("b");
        if (currentQuiz < quizData.length) {
            loadQuiz();

        } else {
            console.log("c");
            //afficher les resulat et recharger
            quiz.innerHTML = `<h3>Vous avez un score de ${score}
            /${quizData.length} questions. </h3>  
            <button onclick="location.reload()">Reload</button>`;
        }
    }
});
///////////////////////////////quiz france////////////////////////


let currentQuiz2 = 0;
let score2 = 0;

const answerEls2 = document.querySelectorAll('input[name="answer2"]');
const questionE2 = document.getElementById('question2');
const a2_text = document.getElementById('a2_text');
const b2_text = document.getElementById('b2_text');
const c2_text = document.getElementById('c2_text');
const d2_text = document.getElementById('d2_text');
const submitBtn2 = document.getElementById('Submit2')


function loadQuiz2() {

    deselectAnswers2();

    const currentQuizData = quizData2[currentQuiz2];
    
    questionE2.innerText = currentQuizData.question;

    a2_text.innerText = currentQuizData.a;
    b2_text.innerText = currentQuizData.b;
    c2_text.innerText = currentQuizData.c;
    d2_text.innerText = currentQuizData.d;
}
loadQuiz2();

function getSelected2() {
    let answer = undefined;

    answerEls2.forEach((answerEl) => {
        if (answerEl.checked) {
            answer = answerEl.id;
        }
    });
    return answer;
}

function deselectAnswers2() {

    answerEls2.forEach((answerEl) => {
        answerEl.checked = false;
    });
}

submitBtn2.addEventListener('click', () => {
    // check  answer
    const answer = getSelected2();
    
    if (answer) {
        console.log(answer === quizData2[currentQuiz].correct)
        if (answer === quizData2[currentQuiz].correct) {
            score2++;
            const correctInput = document.querySelector(`input.answer[id=${answer}]`)
            correctInput.classList.add("success")
        }
        currentQuiz2++;
      
        if (currentQuiz2 < quizData2.length) {
            loadQuiz2();

        } else {
           
            //afficher les resulat et recharger
            quiz2.innerHTML = `<h3>Vous avez un score de ${score2}
            /${quizData2.length} questions. </h3>  
            <button onclick="location.reload()">Reload</button>`;
        }
    }
});
///////////////////////////////quiz japon//////////////////////////
let currentQuiz3 = 0;
let score3 = 0;

const answerEls3 = document.querySelectorAll('input[name="answer3"]');
const questionE3 = document.getElementById('question3');
const a3_text = document.getElementById('a3_text');
const b3_text = document.getElementById('b3_text');
const c3_text = document.getElementById('c3_text');
const d3_text = document.getElementById('d3_text');
const submitBtn3 = document.getElementById('Submit3')


function loadQuiz3() {

    deselectAnswers3();

    const currentQuizData = quizData3[currentQuiz3];
    
    questionE3.innerText = currentQuizData.question;

    a3_text.innerText = currentQuizData.a;
    b3_text.innerText = currentQuizData.b;
    c3_text.innerText = currentQuizData.c;
    d3_text.innerText = currentQuizData.d;
}
loadQuiz3();

function getSelected3() {
    let answer = undefined;

    answerEls3.forEach((answerEl) => {
        if (answerEl.checked) {
            answer = answerEl.id;
        }
    });
    return answer;
}

function deselectAnswers3() {

    answerEls3.forEach((answerEl) => {
        answerEl.checked = false;
    });
}

submitBtn3.addEventListener('click', () => {
    // check  answer
    const answer = getSelected3();
    
    if (answer) {
        if (answer === quizData3[currentQuiz].correct) {
            score3++;
        }
        currentQuiz3++;
      
        if (currentQuiz3 < quizData3.length) {
            loadQuiz3();

        } else {
           
            //afficher les resulat et recharger
            quiz3.innerHTML = `<h3>Vous avez un score de ${score3}
            /${quizData3.length} questions. </h3>  
            <button onclick="location.reload">Reload()</button>`;
        }
    }
});
